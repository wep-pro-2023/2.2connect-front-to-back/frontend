import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import temperatureService from '@/services/temperature'
import { useLoadingStore } from './loading'
export const useTemperatureStore = defineStore('temperature', () => {
  const loadingStore = useLoadingStore()
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)

  async function callconvert() {
    console.log('Store call Convert')
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
    console.log('Store finish call Convert')
  }
  return { valid, celsius, result, callconvert }
})
